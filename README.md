#libgdx-bom

Maven BOM to help creating a libgdx project setup. Only support desktop (any platform, but no android/ios/web support).

Example usage:
```
	<dependency>
		<groupId>org.yah.tool</groupId>
		<artifactId>libgdx-bom</artifactId>
		<version>0.0.1-SNAPSHOT</version>
		<type>pom</type>
	</dependency>
```